<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h1>Buat Account Baru!</h1>
   <h3>Sign Up Form</h3>
   <form action="/welcome" method="POST">
   @csrf
        <label>First name:</label><br><br>
        <input type="text" placeholder="Masukkan Nama Depan" name="fname"><br><br>
        <label>Last name:</label><br><br>
        <input type="text" placeholder="Masukkan Nama Belakang" name="lname"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="LK">Men<br>
        <input type="radio" name="FE">Women<br>
        <input type="radio" name="other">Other<br><br>
        <label>Nationality:</label><br><br>
        <select name="nationality">
            <option value="id">Indonesian</option>
            <option value="sg">Singaporean</option>
            <option value="my">Malaysian</option>
            <option value="au">Australian</option>
        </select><br><br>
        <label>Language Spoken</label><br><br>
            <input type="checkbox">Bahasa Indonesia<br>
            <input type="checkbox">English<br>
            <input type="checkbox">Arabic<br>
            <input type="checkbox">Japanese<br><br>
        <label>Bio:</label><br>
            <textarea name="address" cols="30" rows="5" placeholder="Tuliskan Bio Kamu"></textarea><br>
        <button type="submit">Sign Up!</button>
    </form> 
</body>
</html>