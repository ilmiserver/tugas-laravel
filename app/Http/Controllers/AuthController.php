<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index() {
        return view('data.register');
    }

    public function register(Request $request) {
        $name = $request->fname;
        $lname = $request->lname;
        return view('/welcome', compact("name","lname"));
    }
}
